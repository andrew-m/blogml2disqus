Ruby script to create Disqus custom XML from BlogML
(Also my first ever Ruby script from scratch - bear with me)

I'm working from the BlogML created by my old Subtext install.

[Disqus help page on xml import format](http://help.disqus.com/customer/portal/articles/472150)
[Good blog post explaining minimal permissible xml](http://www.sumedh.info/articles/importing-comments-disqus-xml-database.php)
[REXML tutorial](http://www.germane-software.com/software/rexml/docs/tutorial.html)

